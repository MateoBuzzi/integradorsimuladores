using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptSoul : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    public Transform eje;
    public GameObject pSoul;
    
    private RaycastHit hit;
    public float distance;
    public Vector3 v3;
    public GameObject player;
   
    
    bool jugador;
    bool soul;


    void Start()
    {
        jugador = true;
        soul = false;

        GameObject pSoul = GameObject.FindGameObjectWithTag("Jugador");
        Physics.IgnoreCollision(pSoul.GetComponent<Collider>(), GetComponent<Collider>());
    }

    private void FixedUpdate()
    {
        Move();
    }

    void Update()
    {

       

            if (Input.GetKey(KeyCode.Q))
        {
            jugador = true;
            soul = false;
        }
        if (Input.GetKey(KeyCode.E))
        {
            jugador = false;
            soul = true;
            transform.position = player.transform.position;
            
        }



    }
  
        void Move()
    {
        
        Vector3 RotaTargetZ = eje.transform.forward;
        RotaTargetZ.y = 0;
        if (soul == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetZ), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }
            if(Input.GetKeyDown(KeyCode.Space))
            {

                    rb.AddForce(Vector3.up * 1000);
                
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                rb.AddForce(Vector3.up * -1000);
                var dir = transform.up * speed*Time.deltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }
            
           

            if (Input.GetKey(KeyCode.S))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetZ * -1), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }
           

            Vector3 RotaTargetX = eje.transform.right;
            RotaTargetX.y = 0;
            if (Input.GetKey(KeyCode.D))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetX), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }
          
            if (Input.GetKey(KeyCode.A))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetX * -1), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
            }
          
        }
        else
        {

        }
    }
    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + v3, Vector3.up * -1 * distance);
    }
}