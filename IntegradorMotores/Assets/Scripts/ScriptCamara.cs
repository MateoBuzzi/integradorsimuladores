using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptCamara : MonoBehaviour
{
    public GameObject target;
    public GameObject target2;
    public Vector3 v3;
    public float speed;
    public float maxLook;
    public float minLook;
    public Quaternion camRotation;
    bool seguirtarget;
    bool seguirtarget2;
    ScriptJugador scriptjugador;
    // Start is called before the first frame update
    void Start()
    {
        seguirtarget = true;
        seguirtarget2 = false;
        camRotation = transform.localRotation;
        target2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Cam();

        if( seguirtarget == true)
        {
            transform.position = target.transform.position + v3;
        }
        if(seguirtarget2 == true)
        {
            transform.position = target2.transform.position + v3;
        }
    }
    public void Cam()
    {
        if(Input.GetKey(KeyCode.Q))
        {
            seguirtarget = true;
            seguirtarget2 = false;
            
            target2.SetActive(false);
        }
        if(Input.GetKey(KeyCode.E))
        {
            seguirtarget = false;
            seguirtarget2 = true;
            target2.SetActive(true);
            
        }
        camRotation.y += Input.GetAxis("Mouse X") * speed;
        camRotation.x += Input.GetAxis("Mouse Y") * speed * -1;

        camRotation.x = Mathf.Clamp(camRotation.x, minLook, maxLook);
        transform.localRotation = Quaternion.Euler(camRotation.x, camRotation.y, camRotation.z);



    }   
}
