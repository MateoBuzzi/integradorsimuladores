using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptJugador : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    public Transform eje;
    public bool inground;
    private RaycastHit hit;
    public float distance;
    public Vector3 v3;
    ScriptCamara scCamara;
    bool jugador;
    bool soul;
    public Animator ani;
    bool sprint;
    
    
    void Start()
    {
         jugador = true;
        soul = false;
        sprint = false;
    }

    private void FixedUpdate()
    {
        Move();
    }

    void Update()
    {
        if (Physics.Raycast(transform.position + v3, transform.up * -1, out hit, distance)){
            if(hit.collider.tag == "piso")
            {
                inground = true;
            }
        }
        else
        {
            inground = false;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            jugador = true;
            soul = false;
        }
        if (Input.GetKey(KeyCode.E))
        {
            jugador = false;
            soul = true;
        }

        if(sprint == true)
        {
            ani.SetBool("walk", false);
            ani.SetBool("run", true);
        }
        if (sprint == false)
        {
            ani.SetBool("run", false);
        }

    }

  void Move()
    {
        Vector3 RotaTargetZ = eje.transform.forward;
        RotaTargetZ.y = 0;
        if (jugador == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetZ), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
                if(sprint == false)
                ani.SetBool("walk", true);
            }
            else
            {
                if (inground)
                {
                    rb.velocity = Vector3.zero;
                }
                ani.SetBool("walk", false);
            }

            if (Input.GetKey(KeyCode.S))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetZ * -1), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
                if (sprint == false)
                    ani.SetBool("walk", true);
            }

            Vector3 RotaTargetX = eje.transform.right;
            RotaTargetX.y = 0;
            if (Input.GetKey(KeyCode.D))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetX), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
                if (sprint == false)
                    ani.SetBool("walk", true);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(RotaTargetX * -1), 0.3f);
                var dir = transform.forward * speed * Time.fixedDeltaTime;
                dir.y = rb.velocity.y;
                rb.velocity = dir;
                if (sprint == false)
                    ani.SetBool("walk", true);
            }
            if(Input.GetKeyDown(KeyCode.LeftShift))
            {
                speed = 400;
                sprint = true;
                

            }
            if(Input.GetKeyUp(KeyCode.LeftShift))
            {
                speed = 300;
                sprint = false;
            }
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + v3, Vector3.up * -1 * distance);
    }
}
